package br.com.daniel.enviadorEmail.enviadorEmail;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Configuracao {

	public static Properties getProp() throws IOException {
		Properties props = new Properties();
		try {
			FileInputStream file = new FileInputStream("src/main/resources/application.properties");		
			props.load(file);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return props;
	}
	
	public static String  getHostName() {
		String hostName = null;
		try {
			Properties prop = getProp();
			hostName = prop.getProperty("host.name");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hostName;
	}
	
	public static Integer getSMTPPort() {
		Integer smtpPort = null;
		try {
			Properties prop = getProp();
			smtpPort = Integer.parseInt(prop.getProperty("smtp.port "));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return smtpPort;
	}

	public static String getAuthEmail() {
		String email = null;
		try {
			Properties prop = getProp();
			email = prop.getProperty("auth.email");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return email;
	}

	public static String getAuthPwd() {
		String pwd = null;
		try {
			Properties prop = getProp();
			pwd = prop.getProperty("auth.pwd");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pwd;
	}
}
