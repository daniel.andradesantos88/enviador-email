package br.com.daniel.enviadorEmail.enviadorEmail;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
	
	public void enviar(String emailOrigem, String emailDestinatario, String titulo, String mensagem) {
		
		String hostName = Configuracao.getHostName();
		Integer smtpPort = Configuracao.getSMTPPort();
		String authEmail = Configuracao.getAuthEmail();
		String authPwd = Configuracao.getAuthPwd();
		
		try {
            Email email = new SimpleEmail();
            email.setHostName(hostName);
            email.setSmtpPort(smtpPort);
            email.setAuthenticator(new DefaultAuthenticator(authEmail, authPwd));
            email.setSSLOnConnect(true);

            email.setFrom(emailOrigem);
            email.setSubject(titulo);
            email.setMsg(mensagem);
            email.addTo(emailDestinatario);
            email.send();

        } catch (EmailException e) {
            e.printStackTrace();
        }
	}
}
